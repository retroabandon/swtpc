SWTBUG Notes
============

Memory map:

    E000 E3FF    1K  SWTBUG ROM (1K; MIKBUG was 512b)
    A000 A07F  128b  Processor board RAM
    8000 801F   32b  SS-30 bus I/O area
    0000 ????        Board RAM

Addresses:

    E3F8            Vectors: IRQV, SFE (SWI), NMIV, START (reset)
                    XXX are these somhow duped at top of memory?
    E152  CONTRL    interpreter entry point (jump here to end user program)
    E0D0  START     reset entry point
    A042  STACK     top of SWTBUG stack
    A017  BKLST     breakpoint data (instruction replaced by breakpoint)
    A015  BKPT      breakpoint address
    A00A  PORADD    port address for I/O (see below)
    A006  NMI       NMI handler address
    A004  ENDA      end of data to punch (P)
    A002  BEGA      beginning of data to punch (P)
    A000  IRQ       IRQ handler address


Monitor I/O
-----------

The SS-30 bus provides A0 and A1 to the board, and decodes an enable signal
for each board in slot 0 to 7 covering 4 bytes starting at $8000, $8004,
$8008, ..., $801C.

`PORADD` contains the base I/O address the monitor I/O routines (`INEEE`
and `OUTEEE`) should use; `CONTRL` sets this to $8004.

The monitor self-configures for an MP-S (6850 ACIA) or MP-C (6820 PIA)
interface. XXX work out how it does this.


MIKBUG-Compatible I/O Routines
------------------------------

    E040 LOAD19     E0C8 OUT4HS
    E047 BADDR      E0CA OUT2HS
    E055 BYTE       E0D0 START
    E075 OUTCH      E0E3 CONTRL
    E078 INCH       E19C MCLOFF
    E07B PDATA2     E19D MCL
    E07E PDATA1     E1AC INEEE
    E0BF OUT2H      E1D1 OUTEEE
