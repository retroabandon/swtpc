Southwest Technical Products Corporation: Software and Documentation
====================================================================

This repo contains software and documentation for the 6800 and 6809
computers produced by [SWTPC], the Southwest Technical Products
Corporation. All the material in this repo appears to be abandonware.


References
----------

The `www.swtpc.com/mholley/` web site vanished in 2019-10, reappeared on
`swtpc.org` in 2020-10, and vanished again a year later. Mirrored versions
are on [Michael Evenson's emulator site][mholley-swtpcemu],
[DeRamp][mholley-deramp] and [archive.org][mholley-wayback]. Old URLs are
most easily converted by changing `swtpc.com` to `swtpcemu.com`.

Sources for software etc. in this repo:
- [Michael Holley's SWTPC 6800/6809 documentation collection][mholley-swtpcemu]
- deramp.com [SWTPC Software]: SWTBUG

Additional references:
- [THE SS-50 bus][yakowenk]: pinouts for the SS-50 and SS-30 buses
- Sardis Technologies [SWTPC 6800/6809][sardis] page



<!-------------------------------------------------------------------->
[SWTPC]: https://en.wikipedia.org/wiki/SWTPC

[mholley-swtpcemu]: http://www.swtpcemu.com/mholley/
[mholley-wayback]: https://web.archive.org/web/20181126123347/http://www.swtpc.com/mholley/
[mholley-deramp]: https://deramp.com/swtpc.com/
[SWTPC Software]: https://deramp.com/downloads/swtpc/software/SWTPC%20Software/
[`deramp.com`]: https://deramp.com/

[sardis]:  http://www.sardis-technologies.com/pre-st29/swtpc.htm
[yakowenk]: https://web.archive.org/web/20100617014013/http://www.cs.unc.edu/~yakowenk/swtpc/ss50.html
